cluster_name="Dockerized_DST"
rm -rf $@ /data/pkg/mods
cp -a $@ /data/mods /data/pkg/mods
cp /data/modoverrides.lua /data/data/DST/modoverrides.lua
cp /data/modoverrides.lua /data/data/DST/$cluster_name/modoverrides.lua
cp /data/modoverrides.lua /data/data/DST/$cluster_name/Master/modoverrides.lua
cp /data/modoverrides.lua /data/data/DST/$cluster_name/Master/save/modoverrides.lua
cp /data/modoverrides.lua /data/data/DST/$cluster_name/Caves/modoverrides.lua
cp /data/modoverrides.lua /data/data/DST/$cluster_name/Caves/save/modoverrides.lua
