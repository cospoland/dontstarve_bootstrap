#!/bin/bash
steamcmd_dir="/data/steamcmd"
install_dir="/data/pkg"
cluster_name="Dockerized_DST"
dontstarve_dir="/data/data"

function fail()
{
        echo Error: "$@" >&2
        exit 1
}

function check_for_file()
{
    if [ ! -e "$1" ]; then
            fail "Missing file: $1"
    fi
}

cd "$steamcmd_dir" || fail "Missing $steamcmd_dir directory!"

check_for_file "steamcmd.sh"
check_for_file "$dontstarve_dir/DST/$cluster_name/cluster.ini"
check_for_file "$dontstarve_dir/DST/$cluster_name/cluster_token.txt"
check_for_file "$dontstarve_dir/DST/$cluster_name/Master/server.ini"
check_for_file "$dontstarve_dir/DST/$cluster_name/Caves/server.ini"

./steamcmd.sh +force_install_dir "$install_dir" +login anonymous +app_update 343050 validate +quit

check_for_file "$install_dir/bin"

cd "$install_dir/bin" || fail 

sh "$install_dir/../relink_mods.sh"

run_shared=(./dontstarve_dedicated_server_nullrenderer)
run_shared+=(-cluster "$cluster_name")
run_shared+=(-monitor_parent_process $$)
run_shared+=(-conf_dir DST)
run_shared+=(-persistent_storage_root $dontstarve_dir)

"${run_shared[@]}" -shard Caves  | sed 's/^/Caves:  /' &
"${run_shared[@]}" -shard Master | sed 's/^/Master: /'
